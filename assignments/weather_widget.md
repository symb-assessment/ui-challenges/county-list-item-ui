# Weather Widget UI

Create this UI using HTML and CSS. 

<img src="https://gitlab.com/symb-assessment/ui-challenges/county-list-item-ui/-/raw/main/widget_ui/weather_widget.png" width="500" />

- Width of this UI will be of max 500px and will be responsive for smaller devices
- You can use any similar font
- Assets related to this UI are placed [here](https://gitlab.com/symb-assessment/ui-challenges/county-list-item-ui/-/tree/main/assets)
- You can use bootstrap
